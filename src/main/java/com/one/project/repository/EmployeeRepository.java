package com.one.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.one.project.models.Department;
import com.one.project.models.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String>{
	public List<Employee> findByDepartment(Department d);
	public List<Employee> findByManagerEmployeeId(Employee e);
}
