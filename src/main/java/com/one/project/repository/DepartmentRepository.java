package com.one.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.one.project.models.Department;

public interface DepartmentRepository extends JpaRepository<Department, String> {

}
