package com.one.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.one.project.models.Employee;
import com.one.project.models.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, String>{
	List<Ticket> findByStatus(String status);
	List<Ticket> findByAssignedToAndStatus(Employee e,String status);
}
