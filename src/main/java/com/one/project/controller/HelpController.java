package com.one.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelpController {
	
	@GetMapping(value= {"/","/help"})
	public String helpPage() {
		return "help.html";
	}
}
