package com.one.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.one.project.dto.AssignTicket;
import com.one.project.models.Ticket;
import com.one.project.service.TicketService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value="Ticket Service API") 
@RequestMapping("/ticket")
public class TicketController {

	@Autowired
	TicketService ticketService;

	@GetMapping("/{id}")
	@ApiOperation(value = "Get Tickets Based on Ticket Id", response = Ticket.class)
	public ResponseEntity<Ticket> getById(@PathVariable("id") String id) {
		Ticket t=ticketService.getTicketById(id);
		if(null!=t)
			return new ResponseEntity<Ticket>(t, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);

	}

	@GetMapping("/pending")
	@ApiOperation(value = "Get list of all Pending Tickets", response = Ticket.class)
	public ResponseEntity<List<Ticket>> getPendingTickets() {
		List<Ticket> tList = ticketService.getPendingTickets();
		if (null != tList)
			return new ResponseEntity<List<Ticket>>(tList, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);

	}

	@GetMapping("/pending/department/{dId}")
	@ApiOperation(value = "Get Pending Tickets Based on Department Id", response = Ticket.class)
	public ResponseEntity<List<Ticket>> getPendingTicketsByDepartment(@PathVariable("dId") String dId) {
		List<Ticket> tList = ticketService.getByDepartment(dId);
		if (null != tList && !tList.isEmpty())
			return new ResponseEntity<List<Ticket>>(tList, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/pending/manager/{mId}")
	public ResponseEntity<List<Ticket>> getPendingTicketsByManager(@PathVariable("mId") String mId) {
		List<Ticket> tList = ticketService.getByManager(mId);
		if (null != tList && !tList.isEmpty())
			return new ResponseEntity<List<Ticket>>(tList, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Ticket createTicket(@RequestBody Ticket ticket) {
		return ticketService.saveTicket(ticket);

	}

	@PutMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Ticket updateTicket(@RequestBody Ticket ticket) {
		return ticketService.updateTicket(ticket);
	}
	
	@PutMapping("/assign")
	public ResponseEntity<AssignTicket> assignTicket(@RequestBody AssignTicket at) {
		Ticket t = ticketService.assignTicket(at.gettID(), at.geteId());
		if (null != t)
			return new ResponseEntity<AssignTicket>(at, HttpStatus.ACCEPTED);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);

	}
	
	@PutMapping("/close")
	public ResponseEntity<AssignTicket> closeTicket(@RequestBody AssignTicket at) {
		Ticket t = ticketService.assignTicket(at.gettID(), at.geteId());
		if (null != t)
			return new ResponseEntity<AssignTicket>(at, HttpStatus.ACCEPTED);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);

	}

	@PatchMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Ticket updateTicketFields() {
		return null;

	}
}
