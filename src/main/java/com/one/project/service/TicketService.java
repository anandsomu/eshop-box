package com.one.project.service;

import java.util.List;

import com.one.project.models.Ticket;

public interface TicketService {
	public Ticket getTicketById(String Id);
	public Ticket saveTicket(Ticket t);
	public Ticket assignTicket(String tId,String id);
	public List<Ticket> getPendingTickets();
	public Ticket updateTicket(Ticket t);
	public List<Ticket> getByDepartment(String dId);
	public Ticket closeTicket(String tId, String eID);
	public List<Ticket> getByManager(String mId);
}
