package com.one.project.service;

import com.one.project.models.Employee;

public interface EmployeeService {
	public Employee getEmployee(String id);
	
}
