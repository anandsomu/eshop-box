package com.one.project.service;

import com.one.project.models.Department;

public interface DepartmentService {
	public Department getDepartment(String id);
}
