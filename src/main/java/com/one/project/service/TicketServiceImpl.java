package com.one.project.service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.one.project.models.Department;
import com.one.project.models.Employee;
import com.one.project.models.Ticket;
import com.one.project.repository.DepartmentRepository;
import com.one.project.repository.EmployeeRepository;
import com.one.project.repository.TicketRepository;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {

	final String PENDING = "PENDING";
	final String CLOSED = "CLOSED";

	@Autowired
	TicketRepository ticketRepository;

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	DepartmentRepository departmentRepository;

	@Override
	public Ticket getTicketById(String Id) {
		Optional<Ticket> t = ticketRepository.findById(Id);
		if (t.isPresent())
			return t.get();
		return null;
	}

	@Override
	public Ticket saveTicket(Ticket t) {
		if(null==t)
			return null;
		if (null == t.getCreationTime())
			t.setCreationTime(LocalDateTime.now());
		if (null == t.getStatus())
			t.setStatus(PENDING);
		return ticketRepository.save(t);
	}

	@Override
	public Ticket assignTicket(String tId, String id) {
		Optional<Ticket> t = ticketRepository.findById(tId);
		if (t.isPresent()) {
			Ticket ticket = t.get();
			employeeRepository.findById(id).ifPresent(e -> ticket.setAssignedTo(e));
			/*
			 * Optional<Employee> e=employeeRepository.findById(id); if(e.isPresent())
			 * ticket.setAssignedTo(e.get());
			 */
			return saveTicket(ticket);
		}
		return null;
	}

	@Override
	public List<Ticket> getPendingTickets() {
		return ticketRepository.findByStatus(PENDING);
	}

	@Override
	public Ticket updateTicket(Ticket t) {
		// ticketRepository.findById(t.getId()).ifPresentOrElse(ticket->, emptyAction);
		return null;
	}

	@Override
	public List<Ticket> getByDepartment(String dId) {
		Optional<Department> d = departmentRepository.findById(dId);
		if (d.isPresent())
			return employeeRepository.findByDepartment(d.get()).stream()
					.flatMap(e -> ticketRepository.findByAssignedToAndStatus(e, PENDING).stream())
					.collect(Collectors.toList());
		return null;
	}

	@Override
	public Ticket closeTicket(String tId, String eId) {
		Optional<Ticket> t = ticketRepository.findById(tId);
		if (t.isPresent()) {
			Ticket ticket = t.get();
			employeeRepository.findById(eId).ifPresent(e -> {
				ticket.setClosedBy(e);
				ticket.setClosingTime(LocalDateTime.now());
				ticket.setStatus(CLOSED);
			});
			return saveTicket(ticket);
		}
		return null;
	}

	@Override
	public List<Ticket> getByManager(String mId) {
		Optional<Employee> m=employeeRepository.findById(mId);
		if(m.isPresent()){
			return employeeRepository.findByManagerEmployeeId(m.get()).stream()
			.flatMap(e->ticketRepository.findByAssignedToAndStatus(e, PENDING).stream())
			.collect(Collectors.toList());
		}
		return null;
	}

}
