package com.one.project.dto;

public class AssignTicket {
	private String tID;
	private String eId;
	
	
	
	public AssignTicket() {
		super();
	}
	/**
	 * @return the tID
	 */
	public String gettID() {
		return tID;
	}
	/**
	 * @param tID the tID to set
	 */
	public void settID(String tID) {
		this.tID = tID;
	}
	/**
	 * @return the eId
	 */
	public String geteId() {
		return eId;
	}
	/**
	 * @param eId the eId to set
	 */
	public void seteId(String eId) {
		this.eId = eId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eId == null) ? 0 : eId.hashCode());
		result = prime * result + ((tID == null) ? 0 : tID.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssignTicket other = (AssignTicket) obj;
		if (eId == null) {
			if (other.eId != null)
				return false;
		} else if (!eId.equals(other.eId))
			return false;
		if (tID == null) {
			if (other.tID != null)
				return false;
		} else if (!tID.equals(other.tID))
			return false;
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AssignTicket [tID=" + tID + ", eId=" + eId + "]";
	}
	
	
}
