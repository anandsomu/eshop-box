insert into department values('D001','E-Commerce');
insert into department values('D002','Commerce');
insert into department values('D003','Health');
insert into department values('D004','Technology');
insert into department values('D005','Education');
insert into department values('D006','Public Relations');


insert into employee (id,designation,name,department_id) values('E002','Manager','Katy Price','D002');
insert into employee (id,designation,name,department_id) values('E005','Manager','John Cena','D006');
insert into employee values('E001','Supervisor','Jack Ryan','D002','E002');
insert into employee values('E003','Process Executive','Bryan Olsen','D002','E002');
insert into employee values('E004','Process Executive','Nicholas Tesla','D006','E005');

insert into ticket values('T001',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Self',CURRENT_TIMESTAMP(),'Desc','PENDING','Summary','Issue','E001','E001');
insert into ticket values('T002',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'Self',CURRENT_TIMESTAMP(),'Desc','OPEN','Summary','Issue','E004','E004');

ALTER USER sa SET PASSWORD 'admin123';